BASIC REST API
==============

Very simple REST db interface

REQUIREMENTS
------------

PHP 5.4.0+

INSTALLATION
------------

1. git clone git@bitbucket.org:igorakaamigo/restupid.git PROJECT_DIR
2. Unpack gbapi_init.tar.bz2 file (can be found at Downloads section: https://bitbucket.org/igorakaamigo/restupid/downloads) into PROJECT_DIR
3. cd PROJECT_DIR
4. composer update
5. ./yii migrate
6. cd web
7. php -S localhost:3000

INTERACTING WITH API
--------------------

1. Fetching a user list (JSON):

curl -i -H "Accept:application/json" -H "application/json" -XGET "http://localhost:3000/v1/users"

2. Fetching a user list (XML):

curl -i -H "Accept:application/xml" -H "application/xml" -XGET "http://localhost:3000/v1/users"

3. Fetching a user data (JSON):

curl -i -H "Accept:application/json" -H "application/json" -XGET "http://localhost:3000/v1/users/1?expand=posts"

4. Fetching a user data (XML):

curl -i -H "Accept:application/xml" -H "application/xml" -XGET "http://localhost:3000/v1/users/1?expand=posts"

5. Adding a new user:
curl -i -H "Accept:application/json" -H "Content-Type:application/json" -XPOST  "http://localhost:3000/v1/users" -d '{"email": "aaa@bbb.com", "name": "Aaa Bbb", "birthday": "1964-05-07"}'

6. Update an existing user' data:
curl -i -H "Accept:application/json" -H "Content-Type:application/json" -XPATCH  "http://localhost:3000/v1/users/1" -d '{"email": "aaa@bbb.com", "name": "Aaa Bbb", "birthday": "1964-05-07"}'

7. User removal:
curl -i -H "Accept:application/json" -H "Content-Type:application/json" -XDELETE  "http://localhost:3000/v1/users/1"

8. Fetching a post list (JSON):

curl -i -H "Accept:application/json" -H "application/json" -XGET "http://localhost:3000/v1/posts"

8. Fetching a post list (XML):

curl -i -H "Accept:application/xml" -H "application/xml" -XGET "http://localhost:3000/v1/posts"

10. Adding a new post:
curl -i -H "Accept:application/json" -H "Content-Type:application/json" -XPOST  "http://localhost:3000/v1/posts" -d '{"user_id": "1", "title": "The Post Title", "date":"2016-04-01 00:00", "text": "Blablabla"}'

11. Update an existing post' data:
curl -i -H "Accept:application/json" -H "Content-Type:application/json" -XPATCH  "http://localhost:3000/v1/posts/1" -d '{"user_id": "1", "title": "The Post Title XXX", "date":"2016-04-01 00:00", "text": "Blablabla"}'

12. User removal:
curl -i -H "Accept:application/json" -H "Content-Type:application/json" -XDELETE  "http://localhost:3000/v1/users/1"