<?php
/**
 * Создание таблицы пользователей
 *
 */

use yii\db\Migration;

class m160428_123126_create_users extends Migration
{

    public function up()
    {
        $this->createTable('users', [
            'id'       => $this->primaryKey(),
            'name'     => $this->string(255),
            'email'    => $this->string(255),
            'birthday' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        $this->dropTable('users');
    }

}
