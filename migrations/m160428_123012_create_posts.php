<?php
/**
 * Создание таблицы постов
 *
 */

use yii\db\Migration;

class m160428_123012_create_posts extends Migration
{

    public function up()
    {
        $this->createTable('posts', [
            'id'      => $this->primaryKey(),
            'user_id' => $this->integer(),
            'date'    => $this->dateTime(),
            'title'   => $this->string(255),
            'text'    => $this->text(),
        ]);
    }

    public function down()
    {
        $this->dropTable('posts');
    }

}
