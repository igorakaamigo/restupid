<?php
/**
 * Пользователь гостевой книги
 *
 */

namespace app\modules\v1\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $birthday
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{users}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['birthday'], 'safe'],
            [['name', 'email'], 'string', 'max' => 255],
            [['email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'       => 'ID',
            'name'     => 'Name',
            'email'    => 'Email',
            'birthday' => 'Birthday',
        ];
    }

    /**
     * Посты пользователя
     *
     * @return yii\db\ActiveQueryInterface
     */
    public function getPosts() {
        return $this->hasMany(Post::className(), ['user_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function extraFields() {
        return ['posts'];
    }
}
