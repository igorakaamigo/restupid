<?php

namespace app\modules\v1\controllers;

use yii\rest\ActiveController;
use yii\web\Response;

class PostController extends ActiveController
{

    /**
     * @inheritdoc
     */
    public $modelClass = 'app\modules\v1\models\Post';

}
